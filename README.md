# Hudson Readme


The project is split into three parts.
The Brain device which controls everything, the Minion device which carries
out the tasks the Brain tells it to do, and finally the App which the users
uses to interact with the Brain.

## [Brain](https://gitlab.com/hudson-by-starman/hudson-reset)
The Brain is the center of the project.
It is a small single board computer and it is <i>very</i> secure computer.

The computer has a WiFI dongle connected to it which it uses to host an
Access Point network. This network's name is a randomly generated 4 character
code while the password is another randomly generated 12 character code.
These codes will be written on the box by default, however if the user wishes to
reset this network, they can simply run the script.

The Operating System on the Brain is known as Hudson-OS and the installation
files for this are found in the hudson-reset link above.


On the Brain an [Nginx](https://www.nginx.com/) server interfaces with a 
[Django](https://www.djangoproject.com/) server which runs two
separate web-server on different ports. The ports are randomly generated when
the OS is built.

The first web-server is the connection server. It is entirely inaccessible
except for on the AP network mentioned above. This web-server can accept API
connections from the Android app. The user sends Wifi connection details from
the android app to this webserver which then attempted to connect to the
network provided. It then attempts to use UPNP to forward the port of the
second webserver

The second web-server is the main webserver. It hosts the API connections
required for the Android app to login and get sensor data from connected
devices, as well as send commands to the Brain such as lower the home
temperature or turn on intelligent temperature. This webserver is protected by
mutual-TLS encryption of which the certs are generated when the device gains
access to the internet. This is a very powerful level of encryption.


## Minion
The Minion device plugs into a wall and allows the user to plug a device into
it, either a heater or a light (But soon to be a wide swath of modular options).
It is a micro-controller controlling a relay to switch a device on or off.  
It connects to the Brain over TLS. The encryption key is stored on the ESP8266
so the connection is secure and resistant to 
[Man In The Middle](https://en.wikipedia.org/wiki/Man-in-the-middle_attack)(MITM) 
attacks. The connection is
also done over the Brains own AP network for extra security. It uses authenticated
[MQTT](https://en.wikipedia.org/wiki/MQTT)(Quick, Low latency) messages between 
Minion and Brain. When the user selects to turn a device on or off, a websocket
message is sent to the server which runs a python script to send an MQTT message
to any applicable Minions which are listening, these Minions switch on or off 
depending on the message.


## [App](https://gitlab.com/hudson-by-starman/hudson-android)

Connection to the Brain needs to be as simple as possible. The app was designed
to be usable by any normal user. A normal user must be able to transfer TLS
certs between the App and Brain in a secure way.

This is done through a pairing process. The app will ask the
user to select a nearby Brain (the AP network). The user inputs the password for
the AP network that they found on the box and they securely connect to the AP
network. When they connect to the first server, the app checks if the Brain has
a functional normal internet connection. If not, it will ask the user to enter
their WiFi details. On success the Brain sends the required certs to the users
phone. The app then disconnects from the AP network. The app now connects to the
main server using the newly downloaded certs and connection info and makes or
selects a user. The entire process is a few seconds and is highly automated
and simple for the user.


## BLOCK DIAGRAM
![](media/Block%20Diagram.png)
